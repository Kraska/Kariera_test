import unittest
from selenium import webdriver
import page

class PythonOrgSearch(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.get("https://www.kariera.pl/oferty-pracy-dla-specjalistow-i-menedzerow/")

    def test_search_in_python_org(self):
        
        main_page = page.MainPage(self.driver)
        assert main_page.is_title_matches(), "kariera.pl title doesn't match."
        main_page.search_company_element = "hays"
        main_page.search_city_element = "Poznan"
        main_page.click_go_button()
        search_results_page = page.SearchResultsPage(self.driver)
        assert search_results_page.is_results_found(), "No results found."

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()