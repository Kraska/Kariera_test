from element import BasePageElement
from locators import MainPageLocators

class SearchCompanyElement(BasePageElement):

    locator = "//input[@placeholder='Wyszukaj oferty pracy, specjalista']"

class SearchCityElement(BasePageElement):

    locator = "//input[@placeholder='Miasto']"

class BasePage(object):

    def __init__(self, driver):
        self.driver = driver


class MainPage(BasePage):

    search_company_element = SearchCompanyElement()
    search_city_element = SearchCityElement()

    def is_title_matches(self):
        return "Kariera" in self.driver.title

    def click_go_button(self):
        element = self.driver.find_element(*MainPageLocators.GO_BUTTON)
        element.click()


class SearchResultsPage(BasePage):

    def is_results_found(self):
        return "No results found." not in self.driver.page_source
